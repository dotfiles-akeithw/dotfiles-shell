#!/bin/bash

L_ARDUINO_CMAKE_ROOT=~/opt/Arduino-CMake-NG

if [ -e ${L_ARDUINO_CMAKE_ROOT} ]; then
  export ARDUINO_CMAKE_ROOT=${L_ARDUINO_CMAKE_ROOT}
fi
