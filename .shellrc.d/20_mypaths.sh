#!/bin/bash

if [ -e ~/usr/bin ]; then
  prepend_path ~/usr/bin
fi

if [ -e ~/bin ]; then
  prepend_path ~/bin
fi
