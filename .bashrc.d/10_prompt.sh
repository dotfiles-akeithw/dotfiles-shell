#!/bin/bash

export DEFAULT_PS1="${debian_chroot:+($debian_chroot)}${LIGHT_GREEN}\u@\h:${LIGHT_BLUE}\w${NO_COLOR}"
