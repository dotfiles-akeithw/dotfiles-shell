#!/bin/bash

export ARDUINO_CMAKE_TOOLCHAIN_FILE=${ARDUINO_CMAKE_ROOT}/cmake/Arduino-Toolchain.cmake

arduino_cmake()
{
  cmake -DCMAKE_TOOLCHAIN_FILE=${ARDUINO_CMAKE_TOOLCHAIN_FILE} $1
}
