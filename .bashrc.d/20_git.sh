#!/bin/bash

VCS_PROMPT_SCRIPT=~/usr/bin/vcs_prompt.sh

if [ -e "${VCS_PROMPT_SCRIPT}" ]; then
  . ${VCS_PROMPT_SCRIPT}
  PROMPT_COMMAND="vcs_prompt"
else
  # since there is no VCS script just set the prompt
  export PS1="${DEFAULT_PS1}\n$"
fi

#GIT_PROMPT_SCRIPT=/usr/lib/git-core/git-sh-prompt
#
#if [ -e $GIT_PROMPT_SCRIPT ]; then
#  . $GIT_PROMPT_SCRIPT
#
#  GIT_PS1_SHOWDIRTYSTATE=true
#  GIT_PS1_SHOWCOLORHINTS=true
#  GIT_PS1_UNTRACKEDFILES=true
#
#  PROMPT_COMMAND="__git_ps1 '${PS1}' '\n\\$ ' "
#fi
