#!/bin/bash

spacemacs()
{
  # Check to see if the default emacs is a new enough version
  local MAJOR_VERSION=$(emacs --version | head -n 1 | sed -e 's/GNU Emacs \([0-9]*\)\.\([0-9]*\)\.*\([0-9]*\)*/\1/')
  local MINOR_VERSION=$(emacs --version | head -n 1 | sed -e 's/GNU Emacs \([0-9]*\)\.\([0-9]*\)\.*\([0-9]*\)*/\2/')
  if [ ${MAJOR_VERSION} -gt 24 ]; then
    SPACEMACS='emacs'
    ${SPACEMACS}
  else
    # TODO: If there is an emacs root dir defined then check that version
    echo "Your version of emacs may be to old ${MAJOR_VERSION}.${MINOR_VERSION}"
  fi
}
